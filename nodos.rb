require 'HTTParty'
require 'optparse'

class Todo
    def initialize(todo)
        @todo = todo
    end

    def id
        @todo['id']
    end

    def group_mention?(username)
        (@todo['action_name'] == "mentioned" || @todo['action_name'] == "directly_addressed") && !@todo['body'].include?("@#{username}")
    end

    def is_closed?
        @todo['target']['state'] != "opened"
    end

    def build_failed?
        @todo['action_name'] == "build_failed"
    end
end

class GitLab
    include HTTParty
    base_uri "https://gitlab.com/api/v4"

    def initialize(token)
        @options = { headers: { 'Private-Token': token } }
    end

    def current_username
        self.class.get("/user", @options).parsed_response['username']
    end

    def todos
        todos = []
        page = 1

        begin
            @options.merge!(query: { state: "pending", page: page})
            response = self.class.get("/todos", @options)

            page = response.headers['x-next-page'].to_i
            todos.concat(response.parsed_response.map { |todo| Todo.new todo })
        end until page == 0

        todos
    end

    def mark_as_done(id)
        self.class.post("/todos/#{id}/mark_as_done", @options)
    end
end

options = {}
parser = OptionParser.new do |opts|
    opts.banner = "Usage: nodos.rb <options>"

    opts.on("-g", "--group", "Close group mentions or directly addressed")
    opts.on("-c", "--closed", "Close if issue/MR has been closed or merged")
    opts.on("-b", "--build", "Close if opened due to failed build")
    opts.on("-d", "--dry-run", "Only print the todo ids that will be closed")
    opts.on("-h", "--help", "Print this help") do
        puts opts
        exit
    end
end
parser.parse!(into: options)

# At least one selection criteria must be selected
unless options[:group] || options[:closed] || options[:build]
    puts parser.help
    exit
end

if !ENV.keys.include?("GITLAB_PAT")
    puts "Error: Set the GITLAB_PAT env var"
    puts parser.help
    exit
end

gitlab = GitLab.new(ENV['GITLAB_PAT'])
closed = 0

username = gitlab.current_username
puts "Current User: #{username}"

todos = gitlab.todos
puts "Total todos: #{todos.length}"

todos.each do |todo|
    if (options[:group] == true && todo.group_mention?(username)) ||
       (options[:closed] == true && todo.is_closed?) ||
       (options[:build] == true && todo.build_failed?)
        puts "Todo id: #{todo.id} will be marked as done"
        closed += 1
        gitlab.mark_as_done(todo.id) unless options[:'dry-run']
    end
end

puts "Total todos closed: #{closed}"
